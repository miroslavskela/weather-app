import React, { Component, Fragment } from "react";
import Header from "../partials/Header";
import ApiService from "../../services/ApiService";
import CityList from "./CityList";

class MainPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      names: []
    };
  }

  searchValueChange = value => {
    this.setState({ name: value });
  };

  componentDidMount() {
    document.addEventListener("keydown", this.getWeather);
  }

  getWeather = event => {
    const { names, name } = this.state;
    const nameArr = name.split(", ");

    const nameArrUpper = nameArr.map(name => {
      const letters = name.slice(1);
      const firstLetter = name.charAt(0).toUpperCase();
      return firstLetter + letters;
    });

    if (event.keyCode === 13 && nameArrUpper.length !== 0) {
      event.preventDefault();
      ApiService.fetchCityWeather(nameArrUpper).then(response => {
        this.setState({ names: [...names, ...response], name: response });
      });
    }
  };

  removeCity = id => {
    const { names } = this.state;
    names.splice(id, 1);
    console.log(names);
    this.setState({ names: names });
  };

  checkCityArray = () => {
    if (this.state.names.length !== 0) {
      return (
        <div className="row">
          <div className="container">
            <CityList data={this.state.names} data1={this.removeCity} />
          </div>
        </div>
      );
    }
  };

  render() {
   
      return (
        <Fragment>
          <Header onSearchValueChange={this.searchValueChange} />
          {this.checkCityArray()}
        </Fragment>
      );
    
    
  }
}

export default MainPage;
