import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import MainPage from './app/main/MainPage'
import MainSingleCity from './app/singleCity/MainSingleCity'
import {Switch, Route} from 'react-router-dom'

class App extends Component {
  render() {
    return (
      <Switch>
     <Route exact path="/" component={MainPage}/>
     <Route exact path="/city/name::name" component={MainSingleCity}/>
     </Switch>
    );
  }
}

export default App;
