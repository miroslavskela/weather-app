import React, {Component} from 'react';

class Header extends Component  {
  constructor(props){
    super(props)
    this.state={
      value:'',
    }
  }

  handleChange = event => {
    const inputValue = event.target.value;
    this.props.onSearchValueChange(inputValue);
    this.setState({ value: inputValue });
  };

  render(){
  return (
    <nav>
    <div className="nav-wrapper">
      <form>
        <div className="input-field">
          <input value={this.state.value} onChange={this.handleChange} id="search" placeholder="Enter names of the Cities (London, Belgrade, Rome, Paris etc)" type="search" required/>
        </div>
      </form>
    </div>
  </nav>
  );
}
};

export default Header;