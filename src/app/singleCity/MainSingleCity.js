import React, {Component} from 'react'

class MainSingleCity extends Component{
    constructor(props){
        super(props)
        this.state={
            city:'',
        }
        
    }

    componentDidMount(){
        const city = JSON.parse(localStorage.getItem('cityData'))
        this.setState({city:city})
    }

    render(){
        const {city} = this.state 
        return(
            <h1>{city.name}</h1>
        )
    }
}

export default MainSingleCity