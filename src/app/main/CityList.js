import React from 'react'
import CityItem from './CityItem'

const CityList = ({data, data1}) => {
    return data.map((city, index) => {
        return(
          
           <CityItem data={city} key={index} index={index} data1={data1}/>
          
        )
    })
    
}

export default CityList