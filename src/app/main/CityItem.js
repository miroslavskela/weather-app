import React from 'react';
import {Link} from 'react-router-dom';

const CityItem = ({data, data1, index}) => {
    const saveData = () => {
        localStorage.setItem('cityData', JSON.stringify(data))
    }

    const removeCity = (event) => {
        event.stopPropagation()
        data1(index)
    }
    return(
        <ul className="collection">
        <li className="collection-item avatar">
          <img onClick={removeCity} src="images/yuna.jpg" alt="" className="circle"/>
          <Link to={`/city/name:${data.name}`}><span onClick={saveData} className="title">{data.name}</span></Link>
          <p>{data.humidity} <br/>
            {
                data.temperature
            }
          </p>
         
        </li>
        </ul>
    )
}

export default CityItem