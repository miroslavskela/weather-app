import axios from 'axios';
import City from '../entities/City';


const CITY_ENDPOINT = `https://api.openweathermap.org/data/2.5/weather?q=`

class ApiService {
    static fetchCityWeather = (names) => {
        const requests = names.map(name => axios.get(CITY_ENDPOINT + `${name}&APPID=2bf9445290062fb483685475316e1e0c`));
        return Promise.all(requests)
        .then(responses =>  responses.map(response =>  new City(response.data)))
    }
}

export default ApiService