class City{
    constructor(City){
        this.name = City.name
        this.temperature = City.main.temp
        this.humidity = City.main.humidity
        this.pressure = City.main.pressure
    }
}

export default City